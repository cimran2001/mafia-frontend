import React from "react";
import { Route, Routes } from "react-router-dom";
import Login from "./Login";
import Registration from "./Registration";
import EnterGame from "./EnterGame";
import Profile from "./Profile";

function App() {
    return (
        <>
            <Routes>
                <Route path="/" element={<EnterGame />} />
                <Route path="/login" element={<Login />} />
                <Route path="/registration" element={<Registration />} />
                <Route path="/profile" element={<Profile />} />
            </Routes>
        </>
    );
}

export default App;
