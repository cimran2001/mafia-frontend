import React from "react";
import { useNavigate } from "react-router-dom";
import logo from "./clipart1108103.svg";
import lmafia from "./Logo_Mafia.svg";

function EnterGame() {
    let navigate = useNavigate();
    return (
        <>
            <div className="App">
                <div className="photos">
                    <img src={logo} className="mafia-logo" alt="logo" />
                    <img src={lmafia} className="mafia-logo2" alt="lmafia" />
                </div>

                <br />
                <button
                    onClick={() => {
                        navigate("/login");
                    }}
                >
                    Login
                </button>

                <br /><br /><br /><br />

                <a>Don't have an account?</a>
                <br />
                <button
                    onClick={() => {
                        navigate("/registration");
                    }}
                >
                    Sign up
                </button>
            </div>
        </>
    );
}

export default EnterGame;