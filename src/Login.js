import React from "react";
import { useNavigate } from "react-router-dom";
import logo from "./clipart1108103.svg";
import lmafia from "./Logo_Mafia.svg";

import "./App.css";

function Login() {
    let navigate = useNavigate();
    return (
    <div>
        <button className="goback"
            onClick={() => {
                navigate("/#");
            }}>
            Go Back
        </button>
        <div className="form">
            <div className="photos">
                <img src={logo} className="mafia-logo" alt="logo" />
                <img src={lmafia} className="mafia-logo2" alt="lmafia" />
            </div>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Username" />
            </div>
            <br />
            <div>
                <label>Password: </label>
                <input type="password" placeholder="Password" />
            </div>
            <br />
            <button type="submit">Login</button>

            <p>or</p>

            <button
                onClick={() => {
                    navigate("/registration");
                }}
            >
                Sign up
            </button>
        </div>
    </div>
    );
}

export default Login;

