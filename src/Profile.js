import React from 'react';
import logo from './clipart1108103.svg';
import lmafia from './Logo_Mafia.svg';
import lprofile from './profile.png';

import {useNavigate} from "react-router-dom";
import Registration from "./Registration";
import './Profile.css';
import {buildTimeValue} from "@testing-library/user-event/dist/utils/edit/buildTimeValue";

function Profile() {
    let navigate = useNavigate();
    return (
        <div className="Profile">
            <div className="photos">
                <img src={logo} className="mafia-logo" alt="logo" />
                <img src={lmafia} className="mafia-logo2" alt="lmafia" />
            </div>
            <div>
                <img src={lprofile} className="profile_photo" alt="lprofile" />
                <button type="submit" className="photo_button">Upload photo</button>
            </div>
            <br/>
            <div className="info">
                <label id="log_label">Nickname </label>
                <input type="text" readOnly={true} defaultValue={"Aliyev Tofig"} className="prof_input"/>
            </div>
            <br/>
            <div className="info">
                <label id="log_label">Username </label>
                <input type="text" readOnly={true} defaultValue={"Tofig"} className="prof_input"/>
            </div>
            <br/>
            <div className="info">
                <label id="log_label">Email </label>
                <input type="email" readOnly={true} className="prof_input"/>
            </div>
            <br/>
            <div className="info">
                <label id="log_label">Date of the registration</label>
                <input type="date" readOnly={true} defaultValue={"10.05.2022"} className="prof_input"/>
            </div>
            <br/>
            <button type="submit">Change Password</button>
            <br/>
        </div>
    );
}

export default Profile;